package com.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.WebDriverRunner;
import com.date.DateXPath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Selenide.*;


public class UxcTestrailIoPage {

    //кликнуть по элементу. Два параметра, первый - xpath на что кликаем, второе - xpath того, что появится при клике
    public boolean clickByItem(String xpathElementForClick, String xpathExpectedResult){
        $(By.xpath(xpathElementForClick)).click();
        waitDownloadPage(xpathExpectedResult);
        return $(By.xpath(xpathExpectedResult)).exists();
    }
    //получить url страницы
    public String getUrl(){
        return WebDriverRunner.url();
    }

    //скорее всего в последствии перейдём на метод clickByItem
    private void click(String xpath) {
        $(By.xpath(xpath)).click();
    }


    public void waitDownloadPage(String xpatch){
        $(By.xpath(xpatch))
                .waitUntil(Condition.visible, 10000);
    }

    // Проверка открытия сайта
    public boolean isOpened(){
        waitDownloadPage(DateXPath.buttonLogin);
        return $(By.xpath(DateXPath.buttonLogin)).exists();
    }
    // Проверка элемента на сайте
    public boolean exists(String xpath){
        waitDownloadPage(xpath);
        return $(By.xpath(xpath)).exists();
    }

    //перевести на clickByItem
    // Проверка нажатия кнопки Вход на главной странице
    public  boolean isButtonPressed(){
        click(DateXPath.buttonLogin);
        waitDownloadPage(DateXPath.dialogueLogin);
        return $(By.xpath(DateXPath.dialogueLogin)).exists();
    }

    // Проверка авторизации (НУЖЕН КОРРЕКТНЫЙ ЛОГИН И ПАРОЛЬ ДЛЯ КАЖДОГО КЕЙСА)
    public  boolean isLoggedIn(){
        $(By.xpath(DateXPath.inputLogin)).setValue(DateXPath.loginUser1);
        $(By.xpath(DateXPath.inputPassword)).setValue(DateXPath.passwordUser1);
        click(DateXPath.buttonSignIn);
        waitDownloadPage(DateXPath.logo);
        return $(By.xpath(DateXPath.logo)).exists();
    }
    //более гибкий вариант, чтобы самостоятельно вводить логин и пароль. Лучше перейти к нему
    public  boolean isLoggedIn(String login, String password){
        if($(By.xpath(DateXPath.inputLogin)).exists() && $(By.xpath(DateXPath.inputPassword)).exists() && $(By.xpath(DateXPath.buttonSignIn)).exists())
        { //проверка, появилось ли окно  ввода логина, пароля и кнопки
            $(By.xpath(DateXPath.inputLogin)).setValue(login);
            $(By.xpath(DateXPath.inputPassword)).setValue(password);
            click(DateXPath.buttonSignIn);
            waitDownloadPage(DateXPath.logo);
            return $(By.xpath(DateXPath.logo)).exists();
        }
        return false;
    }


    // Проверка нажатия на Тариф
    public  boolean isTariffPressed(){
        click(DateXPath.tariff);
        waitDownloadPage(DateXPath.buttonChangeTariff);
        return $(By.xpath(DateXPath.buttonChangeTariff)).exists();
    }

    // Проверка нажатия на "10 тарифов в месяц"
    public  boolean isTariffTenPerMonthPressed(){
        click(DateXPath.windowTariffTenPerMonth);
        return (($(By.xpath(DateXPath.checkForColorChange)).exists()) && ($(By.xpath(DateXPath.buttonChangeTariff)).exists()));

    }









}
