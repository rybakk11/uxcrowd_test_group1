package com.date;

import org.apache.poi.hssf.usermodel.*;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class ExcelParser {

    public static ArrayList<ArrayList<String>> parse(String fileName,int page) {
        //инициализируем потоки
        ArrayList<ArrayList<String>> finalList = new ArrayList<>();
        finalList.add(new ArrayList<>());

        //String x =result2.get(0)[1];


        InputStream inputStream = null;
        XSSFWorkbook workBook = null;
        try {
            inputStream = new FileInputStream(fileName);
            workBook = new XSSFWorkbook(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //разбираем первый лист входного файла на объектную модель
        Sheet sheet = workBook.getSheetAt(page);
        Iterator<Row> it = sheet.iterator();
        //проходим по всему листу
        int x=0;
        while (it.hasNext()) {

            Row row = it.next();
            Iterator<Cell> cells = row.iterator();
            while (cells.hasNext()) {
                String result;
                Cell cell = cells.next();
                int cellType = cell.getCellType();
                //перебираем возможные типы ячеек
                switch (cellType) {
                    case Cell.CELL_TYPE_STRING:
                        result = cell.getStringCellValue();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        //System.out.println(String.format("%n", cell.getNumericCellValue()));
                        result = ((Double)(cell.getNumericCellValue())).longValue()+"";
                        break;

                    case Cell.CELL_TYPE_FORMULA:
                        result = cell.getNumericCellValue()+"";
                        break;
                    default:
                        result = "|";
                        break;
                }
                finalList.get(x).add(result);

            }

            x++;
            finalList.add(new ArrayList<>());
        }

        return finalList;
    }

}