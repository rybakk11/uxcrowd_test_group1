package com.date;

public class DateXPath {

    public static String loginUser = "preprod.uxcrowd+client.free@gmail.com";
    public static String passwordUser="Qw123123";

    public static String loginUser1 = "Preprod.uxcrowd+client.incprofile+1@gmail.com";
    public static String passwordUser1="Qw123123";

    public static String loginUser6 = "Preprod.uxcrowd+client.incprofile@gmail.com";
    public static String passwordUser6="Qw123123";

    public static String loginUser66 = "Preprod.uxcrowd+client.unlim@gmail.com";
    public static String passwordUser66="Qw123123";

    public static String loginUser33 = "kot3@yopmail.com";
    public static String passwordUser33="v9ctQX";

    public static String loginUser2 = "agentofasgard32@yopmail.com";
    public static String passwordUser2="0yngHh";

    public static String loginUser3 = "Preprod.uxcrowd+tester.incprofile+3@gmail.com";
    public static String passwordUser3="Qw123123";

    public static String loginAdmin = "preprod.uxcrowd+client.free+3@gmail.com";
    public static String passwordAdmin="Qw123123";

    public static String loginDeveloper = "preprod.uxcrowd+client.free+3@gmail.com";
    public static String passworDeveloper="Qw123123";

    public static String loginUser142="Preprod.uxcrowd+tester.incprofile+4@gmail.com";
    public static String passwordUser142="Qw123123";

    public static String loginCase1_8_5 = "kot222@yopmail.com";
    public static String passwordCase_1_8_5="SSxOsC";


    public static String buttonLogin = "//a[@data-testid=\"Login menu button\"]"; //Кнопка Войти на главной
    public static String dialogueLogin = "//h3[@class=\"modal-lk-title\"]"; // "Вход" в открывающемся меню Ввода логина и пароля
    public static String inputLogin = "//input[@name=\"login\"]"; // Поле для ввода логина
    public static String inputPassword = "//input[@name=\"password\"]"; // Поле для ввода пароля
    public static String buttonSignIn = "//button[@data-testid=\"Login button\"]"; // Кнопка входа в открывшемся меню
    public static String logo = "//div[@class=\"item_menu_logo new_test\"]"; // Лого "Новый тест" (кликабельна)
    public static String tariff = "//p[contains(text(),\"Тариф\")]"; // Подпись "Тариф" тоже можно нажать
    public static String buttonChangeTariff = "//button[@class=\"new-green-btn\"]";//  Кнопка "Изменить тариф"
    public static String buttonSelectTarrif ="//button[@class='sc-bxivhb iaxSpn']"; //кнопка "Выбрать тариф"
    public static String windowTariffTenPerMonth = "//div[contains(text(),\"10 тестов в месяц\")]"; // Кликабельное поле "10 тестов в месяц"
    public static String checkForColorChange = "//div[@class=\"customer-tariff-plan-item ng-scope selected\"]"; // xPath проверки на изменение цвета "10 тестов в месяц"
    public static String checkForActiveButtonChangeTariff = "//button[not(@disabled=\"disabled\")]"; // проверка на активность кнопки Изменить Тариф
    public static String buttonPayment = "//button[contains(text(),\"Да, получить счёт на оплату\")]"; // Кнопка "Да, получить счёт на оплату"
    public static String buttonFillProfile = "//button[contains(text(),\"Заполнить профиль\")]"; // Кнопка "Заполнить профиль"
    public static String buttonReturnMainPage ="//div[@class='sc-bZQynM cQHXTr']//button[@class='sc-bxivhb bJUtjD']";
    public static String requisitesTitle = "//h3[contains(text(),\"Реквизиты для оплаты\")]"; // Заголовок в профиле "Реквизиты для оплаты"
    public static String buttonForCreateNewTest="//span[text()='Создать новый тест']"; //кнопка для создания нового теста

    public static String buttonForTest = "//tr//span[text()='Тест']"; // кнопка для захода в тест "Тест"
    public static String buttonForVideo = "//img[@class=\"sc-qXhiz ihVYHd\"]"; // кнопка для перехода к видео
    public static String inputTranscript = "//input[@placeholder=\"Поиск по транскрипту\"]"; // ввод транскрипта
    public static String enterCounter12 = "//div/span[text()='1/2']"; // количество вхождений слова 1/2
    public static String enterCounter22 = "//div/span[text()='2/2']"; // количество вхождений слова 2/2
    public static String enterCounter11 = "//div/span[text()='1/1']"; // количество вхождений слова 1/1
    public static String arrowLeft = "//div/img[@src=\"/assets/svg/arrowLeft.svg\"]"; // стрелка влево
    public static String arrowRight = "//div/img[@src=\"/assets/svg/arrowRight.svg\"]"; // стрелка вправо
    public static String cross = "//div/img[@src=\"/assets/svg/clear.svg\"]"; // крестик

    public static String buttonDownloadFileSkachatVseResultati = "//a[text()='Скачать все результаты']";

    public static String task = "задание"; // первый ввод в поле транскрипта
    public static String easy = "простым"; // второй ввод в поле транскрипта

    //Поля в графе Профиль
    public static String fieldFIO =          "//input[@name=\"fio\"]";                             // Строка ввода ФИО
    public static String fieldBirthday =     "//input[@id=\"tester-profile-data\"]";               // Строка ввода Даты рождения
    public static String fieldAddress =      "//input[@name=\"city\"]";                            // Строка ввода Адреса проживания
    public static String sexMale =           "//input[@value=\"MALE\"]";                           // Радар: Пол: о Мужской
    public static String sexFemale =         "//input[@value=\"FEMALE\"]";                         // Радар: Пол: о Женский
    public static String familyStatus0 =     "//input[@value=\"NOT_MARRIED\"]";                    // Радар: Семейное положение: Не замужем
    public static String familyStatus1 =     "//input[@value=\"MARRIED\"]";                        // Радар: Семейное положение: В браке
    public static String fieldMonthProfit =  "//input[@id=\"income\"]";                            // Строка ввода месячного дохода
    public static String kidsRadar0 =        "//input[@value=\"NONE\"]";                           // Радар: Кол-во детей: Нет
    public static String kidsRadar1 =        "//input[@value=\"ONE\"]";                            // Радар: Кол-во детей: 1
    public static String kidsRadar2 =        "//input[@value=\"TWO\"]";                            // Радар: Кол-во детей: 2
    public static String kidsRadar3orMore =  "//input[@value=\"FOUR\"]";                           // Радар: Кол-во детей: 3 и больше
    public static String emailSpamCheckBox = "//span[text()=\"E-mail оповещения\"]";               // Чекбокс для Email - уведомлений
    public static String saveButtonProfile = "//div[@class=\"mt-15\"]//button[@class=\"new-green-btn\"]"; // Кнопка Сохранить

    public static String dropMenuEducation = "//span[@id=\"education-button\"]";       // Выпадающее меню выбора образования
    public static String dropMenuSocialStatus = "//span[@id=\"socialStatus-button\"]";  // Выпадающее меню выбора Рода деятельности

    //Поля раздела Реквизиты в редактировании профиля
    public static String requisitesButton = "//section[@class='item_menu_nav ng-scope']//p[@class='item_menu_text']"; //Кнопка профиля Реквизиты
    public static String fieldBankName = "//input[@name='bank']";                                 //Наименование банка
    public static String fieldBIK = "//input[@placeholder='(9 цифр, без пробелов и дефисов)']";   //Поле бик банка
    public static String fieldAccount = "//input[@name='account']";                               //Поле номера расчетного счета
    public static String fieldCorrAccount = "//input[@name='corrAccount']";                       //Поле корреспондентского счета
    public static String fieldPassportNumber = "//input[@placeholder='(10 цифр, без пробелов и дефисов)']";// Поле серии и номера паспорта
    public static String fieldPassportIssuedDate = "//input[@id='tester-profile-data']";          //Поле Дата выдачи паспорта
    public static String fieldPassportIssuedBy  = "//input[@name='issuedBy']";                    //Поле Кем выдан паспорт
    public static String fieldPassportCod = "//input[@placeholder='(6 цифр, без пробелов)']";     //Поле Код подразделения
    public static String fieldINN = "//input[@placeholder='(12 цифр, без пробелов и дефисов)']";  //Поле ИНН
    public static String fieldSNILS = "//input[@placeholder='(11 цифр, без пробелов и дефисов)']";//Поле СНИЛС
    public static String fieldAddress2 = "//input[@name='address']";                              //Поле адрес в реквизитах
    public static String fieldFactAddress = "//input[@name='factAddress']";                       //Поле фиктический адрес
    public static String fieldPhoneNumber = "//input[@name='phone']";                             //Поле номер телефона
    public static String checkBoxRequisites = "//input[@type='checkbox']";                        //Чекбокс приняти условий соглашения
    public static String saveButtonRequisites = "//button[@class='new-green-btn']";               // Кнопка Сохранить в Реквизитах

    //Вводные для заполнения профиля/реквизитов
    //Реквизиты банка
    public static String bankName = "Открытие";                             //Наименование банка
    public static String bankBik = "044525985";                             //БИК банка Открытие
    public static String bankCurrentAccountNumber = "12345679101234567910"; //Расчетного номер
    public static String bankCorrespondentAccount = "30101810945250000297"; //Корреспондетский счет банка Открытие
    //Реквизиты клиента
    public static String fio = "Трататушкин Аристарх Мамедович"; //ФИО клиента
    public static String birthDay = "13.01.1990";                //День Рождения
    public static String passportSeriesAndNumber = "1234111111"; //Серия и номер паспорта
    public static String passportDateOfIssues = "21.12.1993";    //Дата выдачи паспорта
    public static String passportIssuedBy = "Отделением УФМС";   //Кем выдан паспорт
    public static String passportDepartmentCode = "522066";      //Код подразделения
    public static String inn = "123456123465";                   //ИНН
    public static String snils = "12345612346";                  //ИНН
    public static String phoneNumber = "9099099999";             //Номер телефона
    public static String monthProfit = "50 000";                 // Месячный доход клиента
    public static String registrationAddress = "Российская Федерация, Новосибирская область, " +
            "г. Бердск, Территория, изъятая из земель подсобного хозяйства " +
            "Всесоюзного центрального совета профессиональных союзов, для организации " +
            "крестьянского хозяйства, дом 17";                   //Адрес регистрации/фактический
}