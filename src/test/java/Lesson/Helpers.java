package Lesson;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import com.date.DateXPath;
import org.openqa.selenium.By;
import org.testng.Assert;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static io.qameta.allure.Allure.step;

public class Helpers {

    public static void stepClickByItem(String stepName, String xPath) {
        step(stepName, () -> {

            $(byXpath(xPath)).click();
        });
    }
    //Выбирает элемент в выпадающем меню
    public static void stepClickDropMenuItemChooser(String stepName, String xPath, String xPath2 ) {
        step(stepName, () -> {
//            $(By.xpath(xPath)).selectOptionByValue("4");

            $(byXpath(xPath)).click();
            Thread.sleep(500);
            $(byXpath(xPath2)).click();

        });
    }

    public static void pressEnter() {
        $(By.xpath(DateXPath.birthDay)).pressEnter();
    }

    public static void stepClickForCreateNewTest(String stepName, String xPath) {
        step(stepName, () -> {

            $(byXpath(xPath)).click();
        });
    }

    public static void stepXpathSendKeys(String stepName, String xPath, String text) {
        step(stepName, () -> {
            $(byXpath(xPath)).sendKeys(text);
        });
    }

    public static void stepClear(String stepName, String xPath) {
        step(stepName, () -> {
            $(byXpath(xPath)).clear();
        });
    }

    public static void stepOpenPage(String stepName, String url) {
        step(stepName, () -> {
            open(url);
        });
    }
    public static void stepExist(String stepName, String xPath) {

        step(stepName, () -> {
            Assert.assertTrue($(By.xpath(xPath)).exists());
        });
    }

    public static void stepEqualsUrl(String stepName, String actualUrl) {

        step(stepName, () -> {
            Assert.assertEquals(WebDriverRunner.url(),actualUrl);
        });
    }

    public static void stepCompareXlsxFields(String stepName, ArrayList<ArrayList<String>> page, // Не забудьте, что в этот метод отправялются индексы листов
                                             int column, int raw, String expectedValue){       //они начинаются с 0, а индексы экселя с 1

        step(stepName, () -> {
            Assert.assertTrue((page.get(raw).get(column)).equals(expectedValue));
        });
    }

}