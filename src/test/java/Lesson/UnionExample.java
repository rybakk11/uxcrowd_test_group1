package Lesson;

import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class UnionExample {

    @AfterMethod //будет вызываться после каждого метода. Закрывает браузер.
    public void closeBrowser() {
        closeWebDriver();
    }

    /*@AfterMethod
    public void saveInfo() { attachScreenshot("Last"); }

    @Attachment(value = "{attachName}", type = "image/png")
    public static byte[] attachScreenshot(String attachName) {
        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
    }*/

    @BeforeSuite
    public static void setUp() {
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true));
    }


    @AfterSuite
    public static void setDown() {
        SelenideLogger.removeListener("Allure");
    }
}
//    @Attachment(value = "{attachName}", type = "image/png")
//    public static byte[] attachScreenshot(String attachName) {
//        return ((TakesScreenshot) getWebDriver()).getScreenshotAs(OutputType.BYTES);
//    }

