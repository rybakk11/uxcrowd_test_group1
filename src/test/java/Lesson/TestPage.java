package Lesson;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.DownloadsFolder;
import com.codeborne.selenide.FileDownloadMode;
import com.codeborne.selenide.WebDriverRunner;
import com.date.DateXPath;
import com.date.ExcelParser;
import io.qameta.allure.*;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

import static Lesson.Helpers.*;
import static Lesson.Helpers.stepXpathSendKeys;
import static com.codeborne.selenide.Selenide.$;
import static io.qameta.allure.Allure.step;

@Epic("Функциональные тесты")
@Feature("Личный кабинет клиента")

    //UxcTestrailIoPage uxcTestrailIoPage;

public class TestPage extends UnionExample {
    static DownloadsFolder df;

    @Story("Проверка кнопки \"Заполнить профиль\" при изменении Бесплатного тарифа на \"10 тестов в месяц\"")
    @Description("Проверка корректной работы кнопки \"Заполнить профиль\" при изменении Бесплатного тарифа на \"10 тестов в месяц\" и последующего перехода в Профиль")
    @Severity(value = SeverityLevel.MINOR)
    @Owner(value = "Chumakov Dmitriy")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    void case_1_3_13() {

        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Нажать кнопку \"Войти\"", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку \"Войти\"",
                    DateXPath.buttonLogin);
        });
        step("3. Авторизация", () -> {
            stepXpathSendKeys(
                    "3.1 Ввести логин",
                    DateXPath.inputLogin,
                    DateXPath.loginUser6);
            stepXpathSendKeys(
                    "3.2 Ввести пароль",
                    DateXPath.inputPassword,
                    DateXPath.passwordUser6);
            stepClickByItem(
                    "3.3 Нажать кнопку войти",
                    DateXPath.buttonSignIn);
        });
        step("4. Переход к тарифу", () -> {
            stepClickByItem(
                    "4.1 Нажать на кнопку Тариф",
                    DateXPath.tariff);
        });
        step("5. Нажатие на блок 10 тестов в месяц", () -> {
            stepClickByItem(
                    "5.1 Нажатие на блок 10 тестов в месяц",
                    DateXPath.windowTariffTenPerMonth);
            stepExist(
                    "5.2 Проверка смены цвета блока 10 тестов в месяц",
                    DateXPath.checkForColorChange);
            stepExist(
                    "5.3 Проверка активирования и смены цвета кнопки Изменить тариф",
                    DateXPath.checkForActiveButtonChangeTariff);
        });
        step("6. Нажатие на кнопку \"Изменить тариф\"", () -> {
            stepClickByItem(
                    "6.1 Нажать на кнопку \"Изменить тариф\"",
                    DateXPath.buttonChangeTariff);
        });
        step("7. Нажатие на кнопку \"Да, получить счет на оплату\"", () -> {
            stepClickByItem(
                    "7.1 Нажать на кнопку \"Да, получить счет на оплату\"",
                    DateXPath.buttonPayment);
        });
        step("8. Нажатие на кнопку  \"Заполнить профиль\"", () -> {
            stepClickByItem(
                    "8.1 Нажать на кнопку  \"Заполнить профиль\"",
                    DateXPath.buttonFillProfile);
            Thread.sleep(2000);
            stepExist(
                    "8.2 Проверка попадания в профиль",
                    DateXPath.requisitesTitle);
        });
    }

    @Story("Проверка функции \"Поиск по транскрипту\"")
    @Description("Проверка функции \"Поиск по транскрипту\" а также управляющих элементов")
    @Severity(value = SeverityLevel.MINOR)
    @Owner(value = "Chumakov Dmitriy")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    void case_1_9_16() {
        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку \"Войти\"",
                    DateXPath.buttonLogin);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    DateXPath.inputLogin,
                    DateXPath.loginUser33);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    DateXPath.inputPassword,
                    DateXPath.passwordUser33);
            stepClickByItem(
                    "2.4 Нажать кнопку \"Войти\"",
                    DateXPath.buttonSignIn);
            Thread.sleep(5000);
        });
        step("3. Найти и нажать на \"Тест\"", () -> {
            stepExist(
                    "3.1 Найти \"Тест\"",
                    DateXPath.buttonForTest);
            stepClickByItem(
                    "3.2 Нажать на \"Тест\"",
                    DateXPath.buttonForTest);
            Thread.sleep(5000);
        });
        step("4. Нажать кнопку запуска видео респондента", () -> {
            stepClickByItem(
                    "4.1 Нажать кнопку запуска видео респондента",
                    DateXPath.buttonForVideo);
            Thread.sleep(5000);
        });
        step("5. Ввести в поле \"Поиск по транскрипту\" \"задание\"", () -> {
            stepXpathSendKeys(
                    "5.1 Ввести \"задание\"",
                    DateXPath.inputTranscript,
                    DateXPath.task);
            stepExist(
                    "5.2 Проверить что было найдено два вхождения",
                    DateXPath.enterCounter12);
        });
        step("6. Нажать на стрелку \"вперед\"", () -> {
            stepClickByItem(
                    "6.1 Нажать на стрелку \"вперед\"",
                    DateXPath.arrowRight);
            stepExist(
                    "6.2 Проверить что переход был осуществлен",
                    DateXPath.enterCounter22);
            Thread.sleep(1000);
        });
        step("7. Нажать на стрелку \"назад\"", () -> {
            stepClickByItem(
                    "7.1 Нажать на стрелку \"назад\"",
                    DateXPath.arrowLeft);
            stepExist(
                    "7.2 Проверить что переход был осуществлен",
                    DateXPath.enterCounter12);
            Thread.sleep(1000);
        });
        step("8. Нажать на крестик", () -> {
            stepClickByItem(
                    "8.1 Нажать на крестик",
                    DateXPath.cross);
            Thread.sleep(1000);
        });
        step("9. Ввести в поле \"Поиск по транскрипту\" \"простым\"", () -> {
            stepXpathSendKeys(
                    "9.1 Ввести \"задание\"",
                    DateXPath.inputTranscript,
                    DateXPath.easy);
            stepExist(
                    "9.2 Проверить что было найдено одно вхождение",
                    DateXPath.enterCounter11);
        });

    }


    @Story("1.5.20 Проверка модального окна \"Вы использовали все доступные бесплатные тесты\", переход на страницу \"Все тесты\"")
    @Description("Авторизоваться, перейти в профиль клиента, изменить ФИО и сохранить")
    //@DisplayName("Проверка изменения ФИО клиента")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Денис")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_8_5() {

        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    DateXPath.buttonLogin);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginCase1_8_5);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordCase_1_8_5);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");

        });
        step("3. Проверка переход на страницу с файлом\" ", () -> {
            Thread.sleep(1000);
            stepOpenPage("3.1. открытие страницы с файлом","https://preprod.uxcrowd.ru/app-customer-home/tests/59623410");
            Thread.sleep(1000);
        });

        step("4. Проверка загрузки файла ", () -> {
            Thread.sleep(1000);
            stepOpenPage("4.1. открытие страницы с файлом","https://preprod.uxcrowd.ru/app-customer-home/tests/59623410");
            Thread.sleep(1000);
            df=WebDriverRunner.getBrowserDownloadsFolder();
            stepClickByItem("4.2",DateXPath.buttonDownloadFileSkachatVseResultati);
            Thread.sleep(3000);

        });
        step("5. Открытие файла Test_59623410_responses.xlsx и проверка 1 листа",()->{
            ArrayList<ArrayList<String>> list = ExcelParser.parse(df.file("Test_59623410_responses.xlsx").toString(),0);

            stepCompareXlsxFields("5.1 Проверка поля email пользователя", list,0,2,"8776@yopmail.com");
            stepCompareXlsxFields("5.2 Проверка поля email пользователя", list,0,3,"87763@yopmail.com");

        });
        step("6. Открытие второй страницы Test_59623410_responses.xlsx и проверка его",()->{
            ArrayList<ArrayList<String>> list = ExcelParser.parse(df.file("Test_59623410_responses.xlsx").toString(),1);

            stepCompareXlsxFields("6.1 Проверка наличия поля Текст инсайта", list,0,0,"Текст инсайта");
            stepCompareXlsxFields("6.2 Проверка наличия поля Длительность инсайта мм:сс", list,1,0,"Длительность инсайта мм:сс");
        });
    }


    @Story("Проверка кнопки \"Заполнить профиль\" при изменении Бесплатного тарифа на \"10 тестов в месяц\"")
    @Description("Проверка корректной работы кнопки \"Заполнить профиль\" при изменении Бесплатного тарифа на \"10 тестов в месяц\" и последующего перехода в Профиль")
    @Severity(value = SeverityLevel.MINOR)
    @Owner(value = "Chumakov Dmitriy")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    void case_1_8_1() {

        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
            Thread.sleep(4000);
        });
        step("2. Нажать кнопку \"Войти\"", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку \"Войти\"",
                    DateXPath.buttonLogin);
            Thread.sleep(2000);
        });
        step("3. Авторизация", () -> {
            stepXpathSendKeys(
                    "3.1 Ввести логин",
                    DateXPath.inputLogin,
                    DateXPath.loginUser66);
            stepXpathSendKeys(
                    "3.2 Ввести пароль",
                    DateXPath.inputPassword,
                    DateXPath.passwordUser66);
            stepClickByItem(
                    "3.3 Нажать кнопку войти",
                    DateXPath.buttonSignIn);
            Thread.sleep(4000);
        });
        step("4. Скачать файл", () -> {
            stepOpenPage(
                    "4.1 Перейти к нужному тесту",
                    "https://preprod.uxcrowd.ru/app-customer-home/tests/59641017");
            Thread.sleep(2000);
            stepClickByItem(
                    "4.2 Нажать кнопку скачать тест",
                    DateXPath.buttonDownloadFileSkachatVseResultati);
            Thread.sleep(2000);
        });
        step("5. Проверка полей на странице 1", () -> {
            ArrayList<ArrayList<String>> page= ExcelParser.parse("C:\\Users\\zubod\\Downloads\\Test_59641017_responses.xlsx", 0);
            Thread.sleep(2000);
            stepCompareXlsxFields(
                    "5.1 Проверка поля Логин пользователя",
                    page,
                    0,
                    0,
                    "Логин пользователя");
        });
    }

    /*

    @Test
    void Case_1_3_3() {
        uxcTestrailIoPage = open("https://preprod.uxcrowd.ru/", UxcTestrailIoPage.class);
        Assert.assertTrue(uxcTestrailIoPage.isOpened(), "Страница не открылась");
        Assert.assertTrue(uxcTestrailIoPage.isButtonPressed(),"Кнопка не нажалась");
        Assert.assertTrue(uxcTestrailIoPage.isLoggedIn(),"Не залогинились");
        Assert.assertTrue(uxcTestrailIoPage.isTariffPressed(), "Тариф не нажался");

    }
*/

    @Story("Проверка модального окна \"Вы использовали все доступные бесплатные тесты\", переход в раздел \"Тариф\"\n" +
            "Группа 1. Практика")
    @Description("Авторизоваться, перейти в профиль клиента, изменить ФИО и сохранить")
    //@DisplayName("Проверка изменения ФИО клиента")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Денис")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_5_19() {

        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    DateXPath.buttonLogin);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginUser2);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordUser2);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");

        });
        step("3. Проверка нажатия на страницу создания нового теста ", () -> {
            stepClickByItem(
                    "3.1 Нажать кнопку нового теста", DateXPath.buttonForCreateNewTest
            );
        });
        step("4. Выбор нового тарифа", () -> {
            stepClickByItem(
                    "4.1 Нажать кнопку сменить тариф", DateXPath.buttonSelectTarrif
            );
            Assert.assertEquals(WebDriverRunner.url(), "https://preprod.uxcrowd.ru/app-customer-home/tariff");
        });

    }

    @Story("Отображение раздела \"Тариф\" для клиента с тарифом \"Бесплатный\"")
    @Description("Авторизоваться, перейти в профиль клиента, изменить ФИО и сохранить")
    //@DisplayName("Проверка изменения ФИО клиента")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Демир")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_3_1() {
        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    "//a[@id='header-lk-button']");
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginUser1);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordUser1);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");

        });

        step("3. Нажать кнопку кнопка создание нового теста", () -> {
            stepClickByItem("3.1 Нажать кнопку для создания нового теста",
                    DateXPath.buttonForCreateNewTest);
            Thread.sleep(2000);
            System.out.println(WebDriverRunner.url());
           Assert.assertEquals(WebDriverRunner.url(), "https://preprod.uxcrowd.ru/app-customer-home/order");

        });
    }

    @Story("Заполнение корректных данных профиля клиента Физическое лицо")
    @Description("Авторизоваться, перейти в профиль клиента, сохранить незаполненные поля, получить отказ, заполнить личную информацию и сохранить")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Дурасов Антон")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_4_1() {
        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
            Thread.sleep(2000);
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    "//a[@id='header-lk-button']");
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginUser3);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordUser3);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");
        });
        step("3. Заполнение профиля клиента", () ->{
            Thread.sleep(2000);
            stepClickByItem(
                    "3.1 Нажатиее кнопки \"Сохранить\" c негативным результатом из-за незаполненных строк",
                    "//div[@class=\"mt-15\"]//button[@class=\"new-green-btn\"]");
            Thread.sleep(2000);
            stepClickByItem(
                    "3.2 Пол клиента: Радар: \"Мужской\"",
                    "//input[@value=\"MALE\"]");
            Thread.sleep(2000);
            stepClickByItem(
                    "3.3 Социальный статус клиента: Радар: \"Не замужем\"",
                    "//input[@value=\"NOT_MARRIED\"]");
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "3.4 Ввод даты рождения",
                    "//input[@id=\"tester-profile-data\"]",
                    DateXPath.birthDay);
            stepXpathSendKeys(
                    "3.5 Ввод ФИО",
                    "//input[@name=\"fio\"]",
                    DateXPath.fio);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "3.6 Ввод адреса проживания",
                    "//input[@name=\"city\"]",
                    DateXPath.registrationAddress);
            Thread.sleep(2000);
            stepClickDropMenuItemChooser(
                    "3.7 Выбор выпадающего меню \"Образование клиента\"",
                    "//span[@id=\"education-button\"]",
                    "//div[@id=\"ui-id-6\"]");
            Thread.sleep(2000);
            stepClickDropMenuItemChooser(
                    "3.8 Выбор выпадающего меню \"Род деятельности\"",
                    "//span[@id=\"socialStatus-button\"]",
                    "//div[text()='Фрилансер']");
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "3.9 Ввод месячного дохода",
                    "//input[@id=\"income\"]",
                    DateXPath.monthProfit);
            Thread.sleep(2000);
            stepClickByItem(
                    "3.10 Указание кол-ва детей: Радар: 0",
                    "//input[@value=\"NONE\"]");
            Thread.sleep(2000);
            stepClickByItem(
                    "3.11 Заполнение чек-бокса \"Email-оповещения\"",
                    "//span[text()=\"E-mail оповещения\"]");
            Thread.sleep(2000);
            stepClickByItem(
                    "3.12 Нажатие кнопки \"Сохранить\"",
                    "//div[@class=\"mt-15\"]//button[@class=\"new-green-btn\"]");

        });

    }
    @Story("1.5.20 Проверка модального окна \"Вы использовали все доступные бесплатные тесты\", переход на страницу \"Все тесты\"")
    @Description("Авторизоваться, перейти в профиль клиента, изменить ФИО и сохранить")
    //@DisplayName("Проверка изменения ФИО клиента")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Денис")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_5_20() {

        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    DateXPath.buttonLogin);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginUser2);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordUser2);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");

        });
        step("3. Проверка нажатия на \"Вернуться на страницу \"Все тесты\"\" ", () -> {
            stepClickByItem(
                    "3.1 Нажать кнопку нового теста", DateXPath.buttonForCreateNewTest
            );
        });
        step("4. Выбор нового тарифа", () -> {
            stepClickByItem(
                    "4.1 Нажать кнопку сменить тариф", DateXPath.buttonReturnMainPage
            );
            Assert.assertEquals(WebDriverRunner.url(), "https://preprod.uxcrowd.ru/app-customer-home/list-orders");
        });

    }
    @Story("1.4.2 Заполнение корректных данных профиля клиента Юридическое лицо")
    @Description("Авторизоваться, перейти в профиль клиента - Реквизиты, сохранить незаполненные поля, получить отказ, заполнить личную информацию и сохранить")
    @Severity(value = SeverityLevel.CRITICAL)
    @Owner(value = "Дурасов Антон")
    @Link(value = "Uxcrowd", url = "https://preprod.uxcrowd.ru/")
    @Test
    public void case_1_4_2() {
        step("1.Открытие начальной страницы", () -> {
            stepOpenPage(
                    "1.1 Открыть начальную страницу",
                    "https://preprod.uxcrowd.ru/");
            Thread.sleep(2000);
        });
        step("2. Авторизация", () -> {
            stepClickByItem(
                    "2.1 Нажать кнопку войти",
                    "//a[@id='header-lk-button']");
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "2.2 Ввести логин",
                    "//input[@id='login']",
                    DateXPath.loginUser142);
            stepXpathSendKeys(
                    "2.3 Ввести пароль",
                    "//input[@placeholder='Пароль']",
                    DateXPath.passwordUser142);
            stepClickByItem("2.4 Нажать кнопку войти",
                    "//form[@id='form_auth']//button[@class='lk-enter-btn']");
        });
        step("3. Переход в раздел \"Реквизиты\"", () -> {
            stepClickByItem(
                    "Нажать кнопку Реквизиты",
                    "//section[@class='item_menu_nav ng-scope']//p[@class='item_menu_text']");
            Thread.sleep(2000);
        });
        step("4. Заполнение реквизитов клиента", () -> {
            Thread.sleep(2000);
            stepClickByItem(
                    "4.1 Заполнение чек-бокса \"Принятие условий\"",
                    "//input[@type='checkbox']");
            Thread.sleep(2000);
            stepClickByItem(
                    "4.2 Нажатие кнопки \"Сохранить\" c негативным результатом из-за незаполненных строк",
                    "//button[@class='new-green-btn']");
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.3 Ввод даты выдачи паспорта",
                    "//input[@id='tester-profile-data']",
                    DateXPath.passportDateOfIssues);
            stepXpathSendKeys(
                    "4.4 Ввод ФИО",
                    "//input[@name=\"fio\"]",
                    DateXPath.fio);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.5 Наименование банка",
                    "//input[@name='bank']",
                    DateXPath.bankName);
            stepXpathSendKeys(
                    "4.6 БИК банка",
                    "//input[@placeholder='(9 цифр, без пробелов и дефисов)']",
                    DateXPath.bankBik);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.7 Номер расчетного счета",
                    "//input[@name='account']",
                    DateXPath.bankCurrentAccountNumber);
            stepXpathSendKeys(
                    "4.8 Корреспондентский счет",
                    "//input[@name='corrAccount']",
                    DateXPath.bankCorrespondentAccount);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.9 Паспорт серия и номер",
                    "//input[@placeholder='(10 цифр, без пробелов и дефисов)']",
                    DateXPath.passportSeriesAndNumber);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.10 Паспорт кем выдан",
                    "//input[@name='issuedBy']",
                    DateXPath.passportIssuedBy);
            stepXpathSendKeys(
                    "4.11 Паспорт код подразделения ",
                    "//input[@placeholder='(6 цифр, без пробелов)']",
                    DateXPath.passportDepartmentCode);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.12 ИНН",
                    "//input[@placeholder='(12 цифр, без пробелов и дефисов)']",
                    DateXPath.inn);
            stepXpathSendKeys(
                    "4.13 СНИЛС",
                    "//input[@placeholder='(11 цифр, без пробелов и дефисов)']",
                    DateXPath.snils);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.14 Ввод адреса регистрации",
                    "//input[@name=\"address\"]",
                    DateXPath.registrationAddress);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.15 Ввод фактического адреса проживания",
                    "//input[@name='factAddress']",
                    DateXPath.registrationAddress);
            Thread.sleep(2000);
            stepXpathSendKeys(
                    "4.16 Номер телефона",
                    "//input[@name='phone']",
                    DateXPath.phoneNumber);
            Thread.sleep(2000);
        });



    }

}

